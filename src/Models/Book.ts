interface Book {
  id: string;
  title: string;
  description: string;
  author: string;
  publishedAt: string;
  category: string;
  bookings:[];
}

export default Book;
