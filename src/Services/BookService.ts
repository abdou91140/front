import Book from '../Models/Book';

class BookService {
  async fetchBooks(filter: string): Promise<Book[]> {
    try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}${filter}`);
      if (!response.ok) {
        throw new Error(`Failed to fetch books: ${response.statusText}`);
      }
      const data = await response.json();
      return data as Book[];
    } catch (error) {
      console.error('Error fetching books:', error);
      throw new Error('Failed to fetch books');
    }
  }

  async fetchBook(id: string): Promise<Book> {
    try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/${id}`);
      if (!response.ok) {
        throw new Error(`Failed to fetch book details: ${response.statusText}`);
      }
      const data = await response.json();
      return data as Book;
    } catch (error) {
      console.error(`Error fetching book details for ID ${id}:`, error);
      throw new Error('Failed to fetch book details');
    }
  }

  async createBooking(bookId: string, formData: any): Promise<void> {
    try {
      const response = await fetch(`${import.meta.env.VITE_API_URL}/bookings`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          bookId,
          formData,
        }),
      });
      if (!response.ok) {
        throw new Error(`Failed to create booking: ${response.statusText}`);
      }
    } catch (error) {
      console.error('Error creating booking:', error);
      throw new Error('Failed to create booking');
    }
  }
}

export default BookService;
