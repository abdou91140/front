import './App.css';
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import BookList from './Components/BookList';
import BookDetails from './Pages/BookDetails';

const App: React.FC = () => {
  return (
    <>
      <h1>Hupso</h1>
      <Router>
      <Routes>
        <Route path="/" element={<BookList />} />
        <Route path="/books/:id" element={<BookDetails />} />
      </Routes>
    </Router>    </>
  );
};

export default App;
