import React from 'react';
import Book from '../Models/Book';
import { Link } from 'react-router-dom';
import BookingForm from './BookingForm';

interface BookCardProps {
  book: Book;
  imageUrl: string;
}

const BookCard: React.FC<BookCardProps> = ({ book, imageUrl }) => {
  const getYearFromDate = (dateString: string): string => {
    const date = new Date(dateString);
    const formatter = new Intl.DateTimeFormat('us', { month: 'long', year: 'numeric' });
    return formatter.format(date);
  };

  return (
    <div id="card" className="text-left max-w-sm rounded-2xl overflow-hidden shadow-xl bg-gray-200 transition-all duration-300 hover:-translate-y-2 ">

      <div
        className="bg-cover bg-center h-full relative z-0"
        style={{ backgroundImage: `url(${imageUrl})` }}
      >
        <div className="bg-white rounded-t-2xl -bottom-3 absolute min-h-68 h-68 max-w-sm mb-0 px-6 py-4">
          <Link to={`/books/${book.id}`}>
            <div className="font-bold text-3xl mb-1 line-clamp-2 overflow-hidden">{book.title} - {book.author}</div>
            <p><strong>Publication :</strong> {getYearFromDate(book.publishedAt)}</p>

            <div className="pb-1 pt-2">
              <span className="bg-pink-200 rounded-2xl px-2 py-2 text-sm italic text-gray-700 mr-2 ">
                {book.category}
              </span>
            </div></Link>
          <p className="text-gray-700 text-base line-clamp-3 overflow-hidden">{book.description}</p>
          <div className="text-gray-600 text-sm mt-2 z-100">
            {book.bookings.length <1 ? 
            <BookingForm bookId={book.id}/>: <div>Date of booking: {book.bookings.map(booking=><div>from {booking.start_date} to {booking.end_date}</div>)}</div>}
          </div>

        </div>
      </div>

    </div >
  );
};

export default BookCard;
