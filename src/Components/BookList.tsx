import React, { useState, useEffect } from 'react';
import BookCard from './BookCard';
import BookService from '../Services/BookService';
import Book from '../Models/Book';

const BookList: React.FC = () => {
  const [books, setBooks] = useState<Book[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [filterTypes, setFilterTypes] = useState<{ [key: string]: string }>({
    category: '',
    publishedAt: '',
    title: ''
  });

  useEffect(() => {
    const fetchData = async () => {
      const bookService = new BookService();
      try {
        const fetchedBooks = await bookService.fetchBooks(buildFilterString());
        setBooks(fetchedBooks);
        setLoading(false);
      } catch (error) {
        setError(error.message);
        setLoading(false);
      }
    };

    fetchData();
  }, [filterTypes]); 

  const buildFilterString = (): string => {
    const filters = [];
    for (const key in filterTypes) {
      if (filterTypes[key]) {
        filters.push(`/?filter[${key}]=${filterTypes[key]}`);
      }
    }
    return filters.join('&');
  };

  const handleInputChange = (type: string, value: string) => {
    if (value.length >= 2) {
      setFilterTypes({ ...filterTypes, [type]: value });
    }
  };
  const clearFilters = () => {
    setFilterTypes({
      category: '',
      publishedAt: '',
      title: ''
    });
  };
  return (
    <>
      <div className='flex items-center justify-center w-full py-14 '>
        <div className='flex text-center items-baseline'><label htmlFor="categoryInput">By category:</label>
          <input
            type='text'
            id='categoryInput'
            onChange={(e) => handleInputChange('category', e.target.value)}
            placeholder='Enter category'
          /> 
          <label htmlFor="publishedAtInput">By Year:</label>
            <input
              type='date'
              id='publishedAtInput'
              onChange={(e) => handleInputChange('publishedAt', e.target.value)}
              placeholder='Select year'
            /> 
          <label htmlFor="titleInput">By title:</label> 
            <input
              type='text'
              id='titleInput'
              onChange={(e) => handleInputChange('title', e.target.value)}
              placeholder='Enter title'
            />
            <button className='bg-black py-1 px-3 mx-3 text-white mt-2 font-semibold rounded-xl' onClick={clearFilters}>Clear Filters</button>
        </div>
      
      </div>
      <div className="h-full grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-3 gap-6">
        {loading && <div className='h-full pt-24 text-2xl'>Loading...</div>}
        {error && <div className='h-full pt-24 text-2xl text-red-700'>Error: {error}</div>}
        {books.length <= 0 && <div className='h-full pt-24 text-2xl'>No book found...</div>}
        {books.map((book, index) => (
            <BookCard key={index} book={book} imageUrl="https://picsum.photos/200/400" />
        ))}
      </div>
    </>
  );
};

export default BookList;
