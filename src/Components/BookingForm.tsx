import React, { useState } from 'react';
import BookService from '../Services/BookService';

const BookingForm = ({ bookId }) => {
    const [formData, setFormData] = useState({
        bookingDateStart: '',
        bookingDateEnd: '',
    });
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const handleInputChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        setLoading(true);
        try {
            const bookingService = new BookService();
            await bookingService.createBooking(bookId, formData);
        } catch (error) {
            setError('Failed to book: ' + error.message);
        }
        setLoading(false);
    };

    return (
        <form onSubmit={handleSubmit} className="flex justify-center flex-wrap flex-shrink align-center items-center">
            {error && <div>{error}</div>}
            <label className='py-0' htmlFor="bookingDateStart">Date start:</label>
            <input
                type="date"
                id="bookingDateStart"
                name="bookingDateStart"
                value={formData.bookingDateStart}
                onChange={handleInputChange}
                required
            />
            <label htmlFor="bookingDateEnd">Date end:</label>
            <input
                type="date"
                id="bookingDateEnd"
                name="bookingDateEnd"
                value={formData.bookingDateEnd}
                onChange={handleInputChange}
                required
            />
            <button
                type="submit"
                disabled={loading}
                className="w-full text-white font-semibold rounded-lg bg-black px-2 py-2 mt-2"
            >
                {loading ? 'Booking...' : 'Book Now'}
            </button>
        </form>
    );
};

export default BookingForm;
