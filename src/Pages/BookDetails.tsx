import React, { useState, useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import Book from '../Models/Book';
import BookService from '../Services/BookService';



const BookDetails: React.FC = () => {
  const { id = '' } = useParams<{ id: string }>();
  const [book, setBook] = useState<Book | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const getYearFromDate = (dateString: string): string => {
    const date = new Date(dateString);
    const formatter = new Intl.DateTimeFormat('us', { month: 'long', year: 'numeric' });
    return formatter.format(date);
  };

  useEffect(() => {
    const fetchData = async () => {
      const bookService = new BookService();
      try {
        const fetchedBook = await bookService.fetchBook(id);
        setBook(fetchedBook);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching book:', error);
        setLoading(false);
      }
    };

    fetchData();
  }, [id]);

  return (
    <div className='container pt-28'>
      {loading && <div className='h-full pt-24 text-2xl'>Loading...</div>}
      {book &&
        <>
          <div
            className="bg-cover bg-center h-96 "
            style={{ backgroundImage: `url("https://picsum.photos/200/300")` }}
          >  </div><div className="bg-white rounded-t-2xl w-full  min-h-68 h-68 mb-0 px-6 py-4">
            <div className="font-bold text-3xl mb-1 line-clamp-2 overflow-hidden">{book.title} - {book.author}</div>
            <p><strong>Publication :</strong> {getYearFromDate(book.publishedAt)}</p>
            <div className="pb-1 pt-2">
              <span className="bg-pink-200 rounded-2xl px-2 py-2 text-sm italic text-gray-700 mr-2 ">
                {book.category}
              </span>
            </div>
            <p className="text-gray-700 text-base line-clamp-3 overflow-hidden">{book.description}</p>
            <div className="text-gray-600 flex justify-center text-sm mt-2">
              <button className='w-44 max-w-44 text-white  font-semibold rounded-lg bg-black px-2 py-2 mt-2'>Booking</button>
            </div>
          </div>
          <Link to="/">Back to List</Link>
        </>}
    </div>
  );
};

export default BookDetails;
